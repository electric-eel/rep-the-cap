local camera = require("lib/hump/camera")
local vector = require("lib/hump/vector")
require("world")

---@class player
player = {}
player.speed = 200
player.x = 0
player.y = 0
player.dirX = 0
player.dirY = 0
player.isMoving = false
player.collider = world:newRectangleCollider(player.x, player.y, player.x + 64, player.y + 64)
player.collider:setFixedRotation(true)
player.collider:setCollisionClass("Player")
player.camera = camera()

---Player update method.
---@param dt? number Delta time
function player:update(dt)
    local vec = nil

    player.isMoving = false
    player.dirX = 0
    player.dirY = 0

    if love.keyboard.isDown("w") then
        player.dirY = -1
        player.isMoving = true
    end
    if love.keyboard.isDown("s") then
        player.dirY = 1
        player.isMoving = true
    end
    if love.keyboard.isDown("a") then
        player.dirX = -1
        player.isMoving = true
    end
    if love.keyboard.isDown("d") then
        player.dirX = 1
        player.isMoving = true
    end

    player.x = player.collider:getX() - 64 / 2
    player.y = player.collider:getY() - 64 / 2

    vec = vector(player.dirX, player.dirY):normalized() * player.speed

    if vec.x ~= 0 or vec.y ~= 0 then
        player.collider:setLinearVelocity(vec.x, vec.y)
    end

    if not player.isMoving then
        player.collider:setLinearVelocity(0, 0)
    end

    clampCameraView()
end

---Player draw method.
function player:draw()
    love.graphics.setColor(0, 1, 0)
    love.graphics.rectangle("fill", player.x, player.y, 64, 64) -- TODO: Replace with character sprite <09-01-23, EndeyshentLabs> --
end

---Clamp camera view to map bounds.
function clampCameraView()
    local w = love.graphics.getWidth()
    local h = love.graphics.getHeight()

    local mapW = CurrentMap.map.width * CurrentMap.map.tilewidth
    local mapH = CurrentMap.map.height * CurrentMap.map.tileheight

    if player.camera.x < w / 2 then
        player.camera.x = w / 2
    end
    if player.camera.y < h / 2 then
        player.camera.y = h / 2
    end

    if player.camera.x > mapW - w / 2 then
        player.camera.x = mapW - w / 2
    end
    if player.camera.y > mapH - h / 2 then
        player.camera.y = mapH - h / 2
    end
end
