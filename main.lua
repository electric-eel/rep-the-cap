Font = nil

require("utils")
require("map")
require("player")
require("world")
require("UI")

function love.load()
	Font = love.graphics.newFont("res/fonts/dos-vga.ttf", 16)

	CurrentMap.map, CurrentMap.cols = loadMap(nil, "void", 0, 0, player, world)
end

local start_Button = ImageButton:new(
	love.graphics.newImage("res/gfx/start-button.png"),
	love.graphics.newImage("res/gfx/start-button.png"),
	function()
		switchToMap("test1")
	end,
	0,
	0
)

function love.update(dt)
	if CurrentMap.name == "void" and start_Button then
		start_Button:update()
	end

	player.camera:lookAt(player.x, player.y)

	player:update(dt)

	world:update(dt)
end

local bgimg = love.graphics.newImage("res/gfx/bg.png")

function love.draw()
	love.graphics.setColor(1, 1, 1)

	if CurrentMap.name == "void" then
		love.graphics.draw(bgimg, 0, 0, 0, love.graphics.getWidth() / 1920, love.graphics.getHeight() / 1080)
		start_Button:draw()
		return
	end

	player.camera:attach()
	if CurrentMap.map then
		drawMap(CurrentMap.map)
	end

	player:draw()

	if isDebug() then
		world:draw()
	end
	player.camera:detach()
end

function love.keypressed(key)
	if key == "escape" then
		love.event.quit()
	end
end

-- vim:set noet sw=4 ts=4:
