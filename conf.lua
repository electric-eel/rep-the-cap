function love.conf(t)
	t.title = "Rep The Cap! Our game, no copyright."
	t.version = "11.3"

	t.window.width = 800
	t.window.height = 600
	t.window.resizable = true
	t.window.fullscreen = false
	t.window.minwidth = 640
	t.window.minheight = 480
	t.window.highdpi = true
	t.window.vsync = true

	t.modules.joystick = false
	t.modules.touch = false
end
