# Rep The Cap!

Rep The Cap! Our game for ["Fvck Capitalism Jam 2023"](https://itch.io/jam/fuck-capitalism-jam-2023)

## Quick Start

```console
$ git clone --recurse-submodules --recursive https://gitlab.com/electric-eel/rep-the-cap.git
$ cd rep-the-cap
$ love . # Normal mode
$ love . --debug # Debug mode (you can also use `-debug`)
```

## Build-it

```console
Cloning stuff....
$ ./fluid
```
