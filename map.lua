local sti = require("lib/sti/sti")

CurrentMap = {
	map = nil,
	cols = nil,
	name = nil,
}

---Destroy colliders.
---@param cols table Collider table
---@return nil
function destroyCols(cols)
	local i = #cols
	while i > 0 do
		if cols[i] ~= nil then
			cols[i]:destroy()
		end
		table.remove(cols, i)
		i = i - 1
	end
	return nil
end

---Load a specific map. Dev function, do not use this in production.
---@see switch_to_map
---@param colTable? table Collision table of previous map
---@param map string Map name
---@param x? number Player position X
---@param y? number Player position Y
---@param playerObj player
---@param world unknown Windfield world. See `world.lua`
---@return unknown
---@return table
function loadMap(colTable, map, x, y, playerObj, world)
	if colTable then
		destroyCols(colTable)
	end

	if x and y then
		playerObj.x = x + 8
		playerObj.y = y + 8
		playerObj.collider:setPosition(x + 8, y + 8)
	end

	local gameMap = sti("res/map/" .. map .. ".lua")

	local cols = {} -- Colliders
	if gameMap.layers["Cols"] then
		for _, obj in pairs(gameMap.layers["Cols"].objects) do
			local col = world:newRectangleCollider(obj.x, obj.y, obj.width, obj.height)
			col:setType("static")
			col:setCollisionClass("Solid")
			table.insert(cols, col)
		end
	end

	CurrentMap.name = map
	return gameMap, cols
end

---Switch to specific map. Wrapper of `loadMap()`
---@see loadMap
---@param map_name string
function switchToMap(map_name)
	for i, _ in pairs(CurrentMap) do
		if i == "map" then
			CurrentMap.map = destroyMap(CurrentMap.map)
		end
		if i == "cols" then
			CurrentMap.cols = destroyCols(CurrentMap.cols)
		end
	end
	CurrentMap.map, CurrentMap.cols = loadMap(CurrentMap.cols, map_name, 2 * 16, 3 * 16, player, world)
end

---Draw a map.
---@param map unknown Map to draw
function drawMap(map)
	map:drawLayer(map.layers["Under"])
	map:drawLayer(map.layers["Main"])
	map:drawLayer(map.layers["Objects"])
end

---Destroy a map.
---@param map unknown Map to destroy
---@return nil
function destroyMap(map)
	map:removeLayer("Under")
	map:removeLayer("Main")
	map:removeLayer("Objects")
	return nil
end
