local wf = require("lib/wf/windfield")
require("utils")

world = wf.newWorld(0, 0)

world:addCollisionClass("Player")
world:addCollisionClass("Trigger")
world:addCollisionClass("Solid")

if isDebug() then
	world:setQueryDebugDrawing(true)
end
